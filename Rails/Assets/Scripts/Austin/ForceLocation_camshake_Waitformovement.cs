﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceLocation_camshake_Waitformovement : MonoBehaviour
{
    //cameraShake
    public float intensity;
    public float waitTime;

    //ForcedLoaction
    public GameObject lookAt;
    // Use this for initialization
    void Start ()
    {
        //StartCoroutine("CameraShake", waitTime);
        //StartCoroutine("ForcedLocation", waitTime);
        StartCoroutine("WaitMovement", waitTime);
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    IEnumerator WaitMovement(float waitTime)
    {
        Debug.Log("Waiting");
        float elapsed = 0.0f;
        while (elapsed < waitTime)
        {
            elapsed += Time.deltaTime;
            Debug.Log("Waiting");
            yield return null;
        }
        yield return null;
    }

    /// <summary>
    /// Forced the player to look at an object
    /// @author Austin Fritz
    /// </summary>
    IEnumerator ForcedLocation(float waitTime)
    {
        Debug.Log("ForcedLocation");
        float elapsed = 0.0f;
        Quaternion original = Camera.main.transform.rotation;
        Quaternion targetRot = original;

        while (elapsed < waitTime)
        {
            Debug.Log("ForcedLocation");
            elapsed += Time.deltaTime;
            targetRot = Quaternion.LookRotation((lookAt.transform.position - Camera.main.transform.position));
            Camera.main.transform.localRotation = targetRot;

            yield return null;
        }
        Camera.main.transform.rotation = original;
        yield return null;
    }

    /// <summary>
    /// shakes the camera for a given time
    /// @author Austin Fritz
    /// </summary>
    IEnumerator CameraShake(float waitTime)
    {
        Debug.Log("CameraShaking");
        float elapsed = 0.0f;
        Quaternion original = Camera.main.transform.rotation;
        Quaternion current = original;

        while (elapsed < waitTime)
        {
            Debug.Log("CameraShaking");
            elapsed += Time.deltaTime;

            float x = Random.Range(0.0f, intensity);
            float y = Random.Range(0.0f, intensity);

            current.eulerAngles = new Vector3(x, y, 0);

            Camera.main.transform.localRotation = current;

            yield return null;
        }
        Camera.main.transform.rotation = original;
        yield return null;
    }
}
