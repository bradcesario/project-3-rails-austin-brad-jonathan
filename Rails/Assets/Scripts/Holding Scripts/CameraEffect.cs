﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraEffect
{
    public CameraEffectTypes type;
    public float duration;
    public float fadeInTime;
    public float fadeOutTime;
    public float intensity;
    public bool fadeInAndOut;
}
