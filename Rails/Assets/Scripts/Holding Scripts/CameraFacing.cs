﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CameraFacing
{
    public CameraFacingTypes type;
    public GameObject lookAt;
    public GameObject cameraFacingReturn;
    public float time;
    public float lookAtTargetTime;
}
