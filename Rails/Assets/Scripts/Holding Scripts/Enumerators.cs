﻿public enum MovementTypes
{
    STRAIGHT_LINE,
    BEZIER_CURVE,
    LOOK_AND_RETURN,
    WAIT
}

public enum CameraFacingTypes
{
    FREE_MOVEMENT,
    FORCED_LOCATION,
    WAIT
}

public enum CameraEffectTypes
{
    CAMERA_SHAKE,
    SPLATTER,
    FADE,
    WAIT
}