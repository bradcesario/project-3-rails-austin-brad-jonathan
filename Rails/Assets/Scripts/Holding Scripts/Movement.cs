﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Movement
{
    public MovementTypes type;
    public float duration;
    public float waitTime;
    public GameObject endWaypoint;
    public GameObject curveWaypoint;
}
