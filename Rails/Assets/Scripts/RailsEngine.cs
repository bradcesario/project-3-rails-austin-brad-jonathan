﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RailsEngine : MonoBehaviour
{
    //@Author: Bradley Cesario, Austin Fritz, Jonathan Roche
    //10/1/2017
    public List<CameraEffect> cameraEffects;
    public List<Movement> movement;
    public List<CameraFacing> cameraFacing;
    public Camera mainCamera;
    GameObject player;
    public Image splatter;
	public Image fade;
	public float RotateAmount;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        StartCoroutine("MovementSystem");
        StartCoroutine("CameraEffectSystem");
        StartCoroutine("CameraFacingSystem");
    }

    /// <summary>
    /// @Author: Bradley Cesario, 9/30/2017
    /// Checks for the type of movement the designer input and starts that
    /// movement coroutine.
    /// </summary>
    /// <returns></returns>
    IEnumerator MovementSystem()
    {
        for (int i = 0; i < movement.Count; i++)
        {
            switch (movement[i].type)
            {
                case MovementTypes.BEZIER_CURVE:
                    yield return StartCoroutine(BezierCurveMovement(i));
                    break;
                case MovementTypes.LOOK_AND_RETURN:
					yield return StartCoroutine(LookAndReturn(i));
                    break;
                case MovementTypes.STRAIGHT_LINE:
					yield return StartCoroutine (StraightLineMovement (i));
                    break;
                case MovementTypes.WAIT:
                    yield return StartCoroutine(WaitMovement(i));
                    break;
            }
        }
        yield return null;
    }

    /// <summary>
    /// @Author: Bradley Cesario, 9/30/2017
    /// Checks for the camera effect selected by the designer and starts the
    /// desired coroutine.
    /// </summary>
    /// <returns></returns>
    IEnumerator CameraEffectSystem()
    {
        for (int i = 0; i < cameraEffects.Count; i++)
        {
            switch (cameraEffects[i].type)
            {
                case CameraEffectTypes.CAMERA_SHAKE:
                    yield return StartCoroutine(CameraShake(i));
                    break;
                case CameraEffectTypes.FADE:
					yield return StartCoroutine(Fade(i));
                    break;
                case CameraEffectTypes.SPLATTER:
                    yield return StartCoroutine(Splatter(i));
                    break;
                case CameraEffectTypes.WAIT:
                    yield return StartCoroutine(WaitMovement(i));
                    break;
            }
        }
        yield return null;
    }

    /// <summary>
    /// @Author: Bradley Cesario, 9/30/2017
    /// Checks for the type of Camera facing selected by the designer and starts
    /// the coroutine for it.
    /// </summary>
    /// <returns></returns>
    IEnumerator CameraFacingSystem()
    {
        for (int i = 0; i < cameraFacing.Count; i++)
        {
            switch (cameraFacing[i].type)
            {
                case CameraFacingTypes.FORCED_LOCATION:
                    yield return StartCoroutine(ForcedLocation(i));
                    break;
                case CameraFacingTypes.FREE_MOVEMENT:
                    yield return StartCoroutine(FreeMovement(i));
                    break;
                case CameraFacingTypes.WAIT:
                    yield return StartCoroutine(WaitMovement(i));
                    break;
            }
        }
        yield return null;
    }

    //@Author: Austin Fritz
    IEnumerator WaitMovement(int sequenceNumber)
    {
        Debug.Log("Waiting");
        float elapsed = 0.0f;
        //Wait at current location until elapsed time surpasses wait time input.
        while (elapsed < movement[sequenceNumber].waitTime)
        {
            elapsed += Time.deltaTime;
            Debug.Log("Waiting");
            yield return null;
        }
        yield return null;
    }

	IEnumerator LookAndReturn(int sequenceNumber)
	{
		Debug.Log ("Look And Return");
		Vector3 endPos = movement[sequenceNumber].endWaypoint.transform.position;
		float elapsed = 0.0f;
		float rotated = 0.0f;
		while (elapsed < 60f && rotated < RotateAmount)
		{
			elapsed += Time.deltaTime;
			rotated += 1;
			transform.Rotate(0, 1, 0);
			yield return new WaitForEndOfFrame();
		}
		elapsed = 0f;
		rotated = 0f;
		while (elapsed < 60f && rotated < RotateAmount)
		{
			elapsed += Time.deltaTime;
			rotated += 1;
			transform.Rotate(0, -1, 0);
			yield return new WaitForEndOfFrame();
		}
	}

	//@Author: Jonathan Roche, 9/30/2017
	IEnumerator StraightLineMovement(int sequenceNumber)
	{
		Debug.Log ("Straight Line");
		Vector3 endPos = movement[sequenceNumber].endWaypoint.transform.position;
		float elapsed = 0.0f;
		while (elapsed < 60f)
		{
			elapsed += Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, endPos, 2f * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}
		//transform.position = Vector3.MoveTowards(transform.position, endPos, 200f);
		yield return null;
	}

    /// <summary>
    /// @Author: Bradley Cesario, 9/29/2017
    /// Moves the player using a quadratic bezier curve system. Using one control,
    /// or pivot point for the designer to chose and an ending destination.
    /// </summary>
    IEnumerator BezierCurveMovement(int sequenceNumber)
    {
        float step = 0f;
        //Use the set waypoint in the inspector as the final destination.
        Vector3 endPos = movement[sequenceNumber].endWaypoint.transform.position;
        //Player's current starting position.
        Vector3 startPos = transform.position;
        //Point to initiate the bezier curve from.
        Vector3 curvePos = movement[sequenceNumber].curveWaypoint.transform.position;
        //Time to complete the bezier movement.
        float duration = movement[sequenceNumber].duration;

        while (step < duration)
        {
            //Update player's position.
            transform.position = GetPoint(startPos, endPos, curvePos, (step / duration));
            step += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        //Redundancy to ensure player is at end spot for bezier movement.
        transform.position = endPos;
    }
    /// <summary>
    /// Calculates the bezier curve movement arc.
    /// </summary>
    /// <param name="start">Position of where the player is initially standing before movement</param>
    /// <param name="end">Waypoint marker where player will end up once bezier movement is complete.</param>
    /// <param name="curve">The control/pivot point</param>
    /// <param name="t">Time through bezier movement</param>
    /// <returns></returns>
    public Vector3 GetPoint(Vector3 start, Vector3 end, Vector3 curve, float t)
    {
        t = Mathf.Clamp01(t);
        float oneMinusT = 1f - t;
        return oneMinusT * oneMinusT * start + 2f * oneMinusT * t * curve + t * t * end;
    }

    /// <summary>
    /// Forced the player to look at an object
    /// @author Austin Fritz
    /// </summary>
    IEnumerator ForcedLocation(int sequenceNumber)
    {
        Debug.Log("ForcedLocation");
        float elapsed = 0.0f;
        Quaternion original = mainCamera.transform.rotation;
        Quaternion targetRot = original;

        while (elapsed < cameraFacing[sequenceNumber].time)
        {
            Debug.Log("ForcedLocation");
            elapsed += Time.deltaTime;
            targetRot = Quaternion.LookRotation((cameraFacing[sequenceNumber].lookAt.transform.position - mainCamera.transform.position));
            Camera.main.transform.localRotation = targetRot;

            yield return null;
        }
        Camera.main.transform.rotation = original;
        yield return null;
    }

    /// <summary>
    /// @Author: Bradley Cesario, 9/30/2017
    /// Allows the player to move the camera around freely during movement.
    /// </summary>
    IEnumerator FreeMovement(int sequenceNumber)
    {
        float elapsed = 0f;
        //Keeps track of the camera's original facing rotation.
        Quaternion originalPos = mainCamera.transform.rotation;
        while (elapsed < movement[sequenceNumber].duration)
        {
            Debug.Log("Camera should be moveable");
            elapsed += Time.deltaTime;
            //Enable the script to move the camera.
            mainCamera.GetComponent<SimpleMouseLook>().enabled = true;
            yield return null;
        }
        //Once at the end of movement, return camera to original rotation.
        mainCamera.transform.rotation = originalPos;
        //Disable the player from moving camera.
        mainCamera.GetComponent<SimpleMouseLook>().enabled = false;
        yield return null;
    }

    /// <summary>
    /// shakes the camera for a given time
    /// @author Austin Fritz
    /// </summary>
    IEnumerator CameraShake(int sequenceNumber)
    {
        Debug.Log("CameraShaking");
        float elapsed = 0.0f;
        Quaternion original = Camera.main.transform.rotation;
        Quaternion current = original;

        while (elapsed < cameraEffects[sequenceNumber].duration)
        {
            Debug.Log("CameraShaking");
            elapsed += Time.deltaTime;

            float x = Random.Range(0.0f, cameraEffects[sequenceNumber].intensity);
            float y = Random.Range(0.0f, cameraEffects[sequenceNumber].intensity);

            current.eulerAngles = new Vector3(x, y, 0);

            Camera.main.transform.localRotation = current;

            yield return null;
        }
        Camera.main.transform.rotation = original;
        yield return null;
    }


    /// <summary>
    /// @Author: Bradley Cesario, 10/1/2017
    /// Allows a splatter effect to show up in the camera view. Can also fade
    /// the splatter effect in and out.
    /// </summary>
    IEnumerator Splatter (int sequenceNumber)
    {
        //Checks to see if the designer selected the fade in and out option.
        if(cameraEffects[sequenceNumber].fadeInAndOut == true)
        {
            StartCoroutine(FadeInAndOut(true, cameraEffects[sequenceNumber].fadeInTime, cameraEffects[sequenceNumber].fadeOutTime));
        }
        else
        {
            //if fade in and out was not selected. Make visible for set time,
            //using alpha transparency.
            Color tempColor = splatter.color;
            tempColor.a = 0.75f;
            splatter.color = tempColor;
            yield return new WaitForSeconds(cameraEffects[sequenceNumber].duration);
            tempColor.a = 0f;
            splatter.color = tempColor;
        }
        yield return null;
    }

	IEnumerator Fade (int sequenceNumber)
	{
		if(cameraEffects[sequenceNumber].fadeInAndOut == true)
		{
			StartCoroutine(FadeInAndOut(true, cameraEffects[sequenceNumber].fadeInTime, cameraEffects[sequenceNumber].fadeOutTime));
		}
		yield return null;
	}

    /// <summary>
    /// @Author: Bradley Cesario, 10/1/2017
    /// Allows the splatter effect to have a fading in and out effect.
    /// </summary>
    /// <param name="fade">Determines whether splatter is fading in or out</param>
    /// <param name="fadeInTime">How long to fade the splatter effect in</param>
    /// <param name="fadeOutTime">How long to fade the splatter effect out</param>
    IEnumerator FadeInAndOut (bool fade, float fadeInTime, float fadeOutTime)
    {
        //By default it will be true from entering in through the Splatter Coroutine.
        if(fade)
        {
            //Set the alpha for fade in and fade out.
            Color fadeIn = splatter.color;
            fadeIn.a = 0.75f;
            float elaspedTime = 0f;
            Color fadeOut = splatter.color;
            fadeOut.a = 0f;
            while (elaspedTime < fadeInTime)
            {
                //Transitions the images color, through its alpha, for a fade in effect.
                Color fadingIn = splatter.color;
                fadingIn.a = Mathf.Lerp(fadeOut.a, fadeIn.a, (elaspedTime / fadeInTime));
                splatter.color = fadingIn;
                elaspedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            fade = false;
        }
        //Once the image has faded in, it will then commence fading out.
        if(fade == false)
        {
            Color fadeOut = splatter.color;
            fadeOut.a = 0f;
            float elaspedTime = 0f;
            Color fadeIn = splatter.color;
            fadeIn.a = 0.75f;
            while (elaspedTime < fadeOutTime)
            {
                //Transitions the image using its alpha component, to create a fade out effect.
                Color fadingOut = splatter.color;
                fadingOut.a = Mathf.Lerp(fadeIn.a, fadeOut.a, (elaspedTime / fadeOutTime));
                splatter.color = fadingOut;
                elaspedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
